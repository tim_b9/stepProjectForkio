'use strict'

const {src, dest, watch, parallel, series} = require("gulp");
// makes css from scss
const sass = require('gulp-sass')(require('sass'));
// concatinates files to one
const concat = require('gulp-concat');
// debug explains us what is happening
const debug = require("gulp-debug");
// const browserSync = require("browser-sync").create();
// errors)
const notify = require("gulp-notify");
// plumber
const plumber = require('gulp-plumber');
// minify js
let rename = require("gulp-rename");
let uglify = require('gulp-uglify-es').default;
//minify css
const cleanCSS = require('gulp-clean-css');
// source of styles after minifying
const sourcemaps = require('gulp-sourcemaps');
// autoprefixer
const autoprefixer = require('gulp-autoprefixer');
// minify images
const imagemin = require('gulp-imagemin');
// browserSync
const browserSync = require('browser-sync').create();
// del
const del = require('del');
const clean = require('gulp-clean');
// include html
const fileinclude = require('gulp-file-include');



function serve() {
	browserSync.init({
		server: {
			baseDir: "dist/"
		}
	});
}

// from css into scss (only for css and scss files)
function buildStyles() {
	return src('./src/scss/**/main.scss')
		// sourcemaps
		.pipe(sourcemaps.init())
		.pipe(plumber({errorHandler: notify.onError()}))
		.pipe(debug({title: 'Src'}))
		// into css
		.pipe(sass().on('error', sass.logError))
		.pipe(debug({title: 'Css'}))
		// autoprefixer
		.pipe(autoprefixer({
			cascade: false
		}))
		// concat into one
		.pipe(concat('styles.min.css'))
		.pipe(cleanCSS())
		.pipe(debug({title: 'Concat'}))
		// sourcemaps
		.pipe(sourcemaps.write())
		// destination
		.pipe(dest('./dist/css'))
		.pipe(browserSync.stream());
}

function buildScripts() {
	return src('./src/js/**/*.js')
		// sourcemaps
		.pipe(sourcemaps.init())
		.pipe(plumber({errorHandler: notify.onError()}))
		.pipe(debug({title: 'Src'}))
		// all js into one
		.pipe(concat('script.js'))
		.pipe(uglify())
		.pipe(rename({suffix: '.min'}))
		.pipe(debug({title: 'Concat'}))
		// sourcemaps
		.pipe(sourcemaps.write())
		// destination
		.pipe(dest('./dist/js'))
		.pipe(browserSync.stream());
}

// copy index.html to dist folder
function buildPages() {
	return src('./src/index.html')
	.pipe(fileinclude())
		.pipe(dest('./dist'))
		.pipe(browserSync.stream());
}

// copy images to dist folder
function copyImg() {
	return src('./src/img/**/*.*')
		.pipe(imagemin([
			imagemin.gifsicle({interlaced: true}),
			imagemin.mozjpeg({quality: 75, progressive: true}),
			imagemin.optipng({optimizationLevel: 5}),
			imagemin.svgo({
				plugins: [
					{removeViewBox: true},
					{cleanupIDs: false}
				]
			})
		]))
		.pipe(dest('./dist/img'))
		.pipe(browserSync.stream());
}

function cleanDist() {
	return del('dist')
}

// see our changes
function watching() {
	// watch(['./src/html/*.html']).on('change',browserSync.reload)
	watch(['./src/scss/**/*.scss'], buildStyles)
	
	watch(['./src/html/*.html'], buildPages)
	
	watch(['./src/js/**/*.js'], buildScripts)
}

exports.buildStyles = buildStyles;
exports.buildScripts = buildScripts;
exports.buildPages = buildPages;
exports.copyImg = copyImg;
exports.cleanDist = cleanDist;
exports.serve = serve;
exports.watching = watching;

exports.build = series(cleanDist, parallel(buildPages, copyImg, buildStyles, buildScripts,serve, watching))
// exports.dev = series(serve, watching)





// OLD version
// // from css into scss (only for css and scss files)
//  gulp.task("buildStyles", function() {
//     return gulp.src('./src/scss/**/main.scss')
//     // sourcemaps
//     .pipe(sourcemaps.init())
//     .pipe(plumber({errorHandler: notify.onError()}))
//     .pipe(debug({title:'Src'}))
//     // into css
//       .pipe(sass().on('error', sass.logError))
//       .pipe(debug({title:'Css'}))
//       // autoprefixer
//     .pipe(autoprefixer({
// 			cascade: false
// 		}))
//       // concat into one
//       .pipe(concat('styles.min.css'))
//       .pipe(cleanCSS())
//       .pipe(debug({title:'Concat'}))
//       // sourcemaps
//       .pipe(sourcemaps.write())
//       // destination
//       .pipe(gulp.dest('./dist/css'))
//       .pipe(browserSync.stream());
//   });

// gulp.task("buildScripts", function() {
//   return gulp.src('./src/js/**/*.js')
//    // sourcemaps
//    .pipe(sourcemaps.init())
//   .pipe(plumber({errorHandler: notify.onError()}))
//   .pipe(debug({title:'Src'}))
//   // all js into one
//   .pipe(concat('script.js'))
//   .pipe(uglify())
//   .pipe(rename({suffix:'.min'}))
//     .pipe(debug({title:'Concat'}))
//     // sourcemaps
//     .pipe(sourcemaps.write())
//     // destination
//     .pipe(gulp.dest('./dist/js'))
//     .pipe(browserSync.stream());
// });

// copy index.html to dist folder
// gulp.task('buildPages', function(){
//   return gulp.src('./src/html/**/*.html')
//   .pipe(gulp.dest('./dist/html'))
//   .pipe(browserSync.stream());
// })

// // copy images to dist folder
// gulp.task('copyImg', function(){
//   return gulp.src('./src/img/**/*.*')
//   .pipe(imagemin([
//     imagemin.gifsicle({interlaced: true}),
//     imagemin.mozjpeg({quality: 75, progressive: true}),
//     imagemin.optipng({optimizationLevel: 5}),
//     imagemin.svgo({
//       plugins: [
//         {removeViewBox: true},
//         {cleanupIDs: false}
//       ]
//     })
//   ]))
//   .pipe(gulp.dest('./dist/img'))
//   .pipe(browserSync.stream());
// })

//   gulp.task('cleanDist', function () {
//     return del('dist')
// });
